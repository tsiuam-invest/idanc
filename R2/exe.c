/*!\file 
 * Nodo receptor etiqueta R2.
 * uso: ./exe
 */

#include "../includes/config.h"
int main(void)
{ 

   dauto();  
   SOCKET a = nuevo_socket(IP_N2, PUERTO_A);
   SOCKET b = nuevo_socket(IP_T1, PUERTO_B);
   SOCKET c = nuevo_socket(IP_R3, PUERTO_A);
   p2p_codificador(&a,&b,&c);
   /// Recupera el vector fuente
   recupera("recuperado_"FILE_NAME);
   
   /// Limpiar
   remove("tmp_xor");
   guardar_bitacora("R2");

   return 0;
}

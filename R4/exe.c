/*!\file 
 * Nodo receptor etiqueta R4.
 * uso: ./exe
 */
#include "../includes/config.h"
int main(void)
{ 
   
   dauto();  
   SOCKET a = nuevo_socket(IP_N4, PUERTO_A);
   SOCKET b = nuevo_socket(IP_T2, PUERTO_A);
   SOCKET c = nuevo_socket(IP_R3, PUERTO_B);
   p2p_codificador(&a,&b,&c);
   /// Recupera el vector fuente
   recupera("recuperado_"FILE_NAME);
   
   /// Limpiar
   remove("tmp_xor");
   guardar_bitacora("R4");

   return 0;
}

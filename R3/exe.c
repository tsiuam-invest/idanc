/*!\file 
 * Nodo receptor etiqueta R3. 
 * uso: ./exe
 */

#include "../includes/config.h"
int main(void)
{ 
 
   dauto();  
   SOCKET a = nuevo_socket(IP_DISPERSAL, PUERTO_D3);
   SOCKET b = nuevo_socket(IP_R2, PUERTO_PREDETERMINADO);
   SOCKET c = nuevo_socket(IP_R4, PUERTO_PREDETERMINADO);
   p2p(&a,&b,&c);
   /// Recupera el vector fuente
   recupera("recuperado_"FILE_NAME);
   guardar_bitacora("R3");

   return 0;
}

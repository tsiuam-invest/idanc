/*!\file
 * Módulo: fuenteN.c
 * Crea nodos fuentes Nsub[1-5].
 *
 * uso: ./fuenteN _etiqueta_
 * 
 * eje: ./fuenteN 1
 */

#include "includes/config.h"            


int main(int argc, char *argv[])  {
	
    dauto();
    SOCKET receptor;
    switch(*argv[1]) {
	 case '1':receptor = nuevo_socket (IP_DISPERSAL,PUERTO_D1);
                  break;
	 case '2':receptor = nuevo_socket (IP_DISPERSAL,PUERTO_D2);
	          break;
	 case '4':receptor = nuevo_socket (IP_DISPERSAL,PUERTO_D4);
	          break;
	 case '5':receptor = nuevo_socket (IP_DISPERSAL,PUERTO_D5);
	          break;
	 default:ERROR("Argumentos admitidos:1-5");
	         break;
	   }

    SOCKET emisorA = nuevo_socket(LOCAL_HOST, PUERTO_A);
    SOCKET emisorB = nuevo_socket(LOCAL_HOST, PUERTO_B);
    multicast_receptor_emisor(&receptor, &emisorA, &emisorB);
    
    return 0;

}

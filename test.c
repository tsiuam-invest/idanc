/*!\file
 *  Módulo: dispersal.c 
 *  Crea 5 vectores dispersos.
 *  Vector fuente especificado en includes/config.h.
 *  Trasmisión acorde en includes/config.h.
 *  uso: ./dispersal
 */
#include "includes/config.h"            

int main(int argc, char *argv[])
{

    //printf("%d\n",(int)obtener_tam_archivo(argv[1]));

    ofuscar_archivo(argv[1],"cifrado");
    des_ofuscar_archivo("cifrado", argv[2]);
    return 0;
}

/*!\file
 *  Módulo: intermedioC.c
 *  Creación de nodos intermediarios C[1,2]
 *  
 *  uso: ./intermedioC _etiqueta_
 *  
 *  eje: ./intermedioC 1
 */

#include "includes/config.h"
//------------------------------------------------------------------
int main(int argc, char **argv)
{  
    dauto();  
    assert(argv[1]);
    char mod = *argv[1];
    SOCKET nodoA, nodoB, emisor;
    switch(mod) {
	 case '1':
	    nodoA = nuevo_socket (IP_N1, PUERTO_B);
	    nodoB = nuevo_socket (IP_N2, PUERTO_B);
            break;
	 case '2':
	    nodoA = nuevo_socket (IP_N4, PUERTO_B);
	    nodoB = nuevo_socket (IP_N5, PUERTO_B);
	    break;
	 default: ERROR("Argumentos admitidos:1 o 2");
	    break;
	   }
    emisor = nuevo_socket(LOCAL_HOST, PUERTO_PREDETERMINADO);
    multicast_emisor (&emisor, &nodoA, &nodoB);

   return 0;
}

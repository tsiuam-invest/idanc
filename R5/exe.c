/*!\file 
 * Nodo receptor etiqueta R5.
 * uso: ./exe
 */
#include "../includes/config.h"
int main(void)
{ 
   dauto();  
   SOCKET a = nuevo_socket(IP_N5, PUERTO_A);
   SOCKET b = nuevo_socket(IP_T2, PUERTO_B);
   SOCKET c = nuevo_socket(IP_R1, PUERTO_PREDETERMINADO);
   p2p_codificador(&a,&b,&c);
   /// Recupera el vector fuente
   recupera("recuperado_"FILE_NAME);
   
   /// Limpiar
   remove("tmp_xor");
   guardar_bitacora("R5");
   
   return 0;
}

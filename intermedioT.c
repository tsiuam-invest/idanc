 /*!\file
  * Módulo: intermedioT.c
 *  Creación de nodos intermediarios T[1,2]
 *
 *  uso: ./intermedioT _etiqueta_
 *
 *  eje: ./intermedioT 1
 */

#include "includes/config.h"

int main(int argc, char *argv[])  {
   dauto();  

    SOCKET receptor = define_receptorT(*argv[1]);
    
    SOCKET emisorA = nuevo_socket(LOCAL_HOST, PUERTO_A);
    SOCKET emisorB = nuevo_socket(LOCAL_HOST, PUERTO_B);
    multicast_receptor_emisor(&receptor, &emisorA, &emisorB);
  
    return 0;
 
}

#ifndef DEBUG_H 
#define DEBUG_H
/**
-----------------------------------------------------------------------------+
| Simple dbug header                                                         |
| author->keyslot                                                            |
 ----------------------------------------------------------------------------+
**/
#include <stdio.h>           /* I.O. streams                                */
#include <stdlib.h>          /* To call exit                                */
#include <string.h>          /* To get the last errno info                  */
#include <stdarg.h>          /* To get __VA_ARGS__                          */
#include <errno.h>           /* To get the last errno                       */

#if DEBUG

#define __DEBUG_EXT__       ".log"
#define __DEBUG_PATH__      __FILE__""__DEBUG_EXT__
#define __DEBUG_STREAM__    stderr

#define dauto()            __debug_open_log__(__DEBUG_PATH__,"w")
#define dset(file,mode)    __debug_open_log__(file,mode)
#define dclose()           __debug_close_log__()

#define dpoint() __debug_write_point__()

#define dprintf(...)(\
{\
	fprintf(__debug_file__(),"\n"__DEBUG_MSG__"\tdprintf\t" __VA_ARGS__);\
})
void __debug_close_log__(void);
void __debug_open_log__(char *path,char *mode);
int  __debug_point__(char *process,int line,char *status,FILE *out);

#define __debug_write_point__()(\
{\
	__debug_point__(\
		 	(char *)__func__,\
	                        __LINE__,\
			        __FILE__,\
			 __debug_file__()\
		       );\
})


#else
#define dauto()      NULL
#define dset(x,y)    NULL
#define dclose()     NULL 
#define dprintf(...) __debug_point_line__(0)
#define dpoint()  __debug_point_line__(__LINE__)
#endif

#ifndef __debug__lambda__
#define __debug__lambda__(...) #__VA_ARGS__
#endif

#ifndef __degug_write_error_code__
#define __debug_write_error_code__(type,error)({\
	__debug_write_error__(\
	                      type,\
			      __debug__lambda__(error),\
	                      (char *)__func__,\
			      __FILE__,\
			      __LINE__,\
			      __debug_file__()\
			     );\
})
#endif

#ifndef dexit_if
#define dexit_if(e)({\
  if(e){\
	  __debug_write_error_code__(__DEBUG_MSG__"\texit_by_",e);\
	  exit(EXIT_FAILURE);\
  }\
})
#endif 

#ifndef dwarning_if
#define dwarning_if(e)( {\
        if(e) { __debug_write_error_code__(__DEBUG_MSG__"\twarning_by_",e); }\
})
#endif 

#define __DEBUG_MSG__ "Log"
#define __DEBUG_WARNG_MSG__ "Error!"
#define __DEBUG_ERROR_INFO__ strerror(errno)



FILE *__debug_file__(void);
int   __debug_point_line__(int macro);
void  __debug_write_error__(char *type,char *expr,char *process,char *in,
                            int line,FILE *out);

#endif



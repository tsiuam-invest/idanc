#ifndef SPANISH_H
#define SPANISH_H

/*!\file
 * Mensajes de salida en versión Inglés
 */

#define STR_GRABAR_BUFFER_ARCHIVO_1   "%s/%d_%d_%d_%d:%d:%d_%s"
#define STR_GRABAR_BUFFER_ARCHIVO_2   "Recording file %s\n"

#define STR_PROCESAR_ARCHIVO_1   "\rSerializatting..."
#define STR_PROCESAR_ARCHIVO_2   "%s/%s"
#define STR_PROCESAR_ARCHIVO_3   "\nVector info:"
#define STR_PROCESAR_ARCHIVO_4   "Name: |%s| + 1  = %d\n"
#define STR_PROCESAR_ARCHIVO_5   "Size of file: %d Bytes\n"
#define STR_PROCESAR_ARCHIVO_6   "Final length: %d + %d (int) + %d    "
#define STR_PROCESAR_ARCHIVO_7   "= %d Bytes\n"

#define STR_CREAR_DIRECTORIO_1   "\nMaking the dir: %s\n"

#define STR_GUARDAR_PAQUETE_1   "File name: %s\n"
#define STR_GUARDAR_PAQUETE_2   "\nFile size:  %d\n"

#define STR_LEER_PAQUETE_1   "Reading dataframes (socket): %d\n" 

// Header
#define STR_CREAR_CARPETA "Creating dir..."
#define STR_CONTROL_HILOS_1  "\r... Set up threads"
#define STR_LANZAR_HILOS_1   "\rTrigger threads...."

// LOGS

#define STR_IMPRIME_TIEMPO_TRANS_1 "Elapsed time: %f (secs) \n on line  %d, from the file %s\n"
#define STR_GUARDAR_BITACORA_1 "\rCreating dir: %s\n"
#define STR_GUARDAR_BITACORA_2 "\rSaving log ....                            "
#define STR_DETECTAR_ATAQUE_1 "Possible pollution attack!"


// SOCKET

#define STR_RESETEA_SOCKET_1 "Error, setting socket: "
#define STR_CONFIGURAR_IP_1 "Error, setting IP socket"

#define STR_PREPARA_SERVIDOR_1 "\rError bind process"
#define STR_PREPARA_SERVIDOR_2 "\rCorrect process ... \nServer listening ..."

#define STR_CONECTAR_1 "\rError by connect : waitting attempt ..."
#define STR_CONECTAR_2 "\rCorrect process : Linked."

#define STR_ESCUCHAR_1 "Error bind process: "
#define STR_LEER_1 "%d"
#define STR_LEER_2 "Error while reading dataframes from socket: "

#define STR_MOSTRAR_CLIENTE_1 "\nIP address incoming: %s\nPort: %lo\n"
#define STR_ACEPTAR_1 "Attemp connections ..."
#define STR_ACEPTAR_2 "Error when attemp to accept:"
#define STR_INICIAR_SERVIDOR_1 "Starting server"
#define STR_INICIAR_SERVIDOR_2 "Closing server"
#define STR_DESTRUIR_SOCKET_1 "Cleaning buffer..."

#define STR_MULTICAST_RECEPTOR_1 "\n\n"
#define STR_MULTICAST_RECEPTOR_2 "\rReading  %d bytes incoming                                "
#define STR_MULTICAST_RECEPTOR_3 "\rDecoding %d bytes "
#define STR_MULTICAST_RECEPTOR_4 "Node A, catchs : %d bytes\n"
#define STR_MULTICAST_RECEPTOR_5 "Node B, catchs : %d bytes\n"

#define STR_MULTICAST_EMISOR_1 "\rIMPORTANT: Coding   %d                        "
#define STR_MULTICAST_RECEPTOR_EMISOR_1 "\rForwarding = %d bytes, total = %d             "
#define STR_MULTICAST_RECEPTOR_EMISOR_2 "SOCKET READING"

// SEGURIDAD //
// CIFRADO
#define STR_LIBERAR_MANEJADOR_SEGURO_1 puts("\nApplying Security!");
#define STR_CONFIGURAR_LLAVE_CIFRADO_1 puts("\nSucessfull key setup.\n");
#define STR_CREAR_MANEJARO_SEGURO_1 fputs ("Error from libgcrypt lib: \n", stderr);
#define STR_CREAR_MANEJARO_SEGURO_2 puts("Sucessfull cypher setup!");
#define STR_CIFRAR_BUFFER_1 puts("\nEncrypting\n");
#define STR_DES_CIFRAR_BUFFER_1 printf("\nSize of vector encrypted: %d\n", (int) tam);

// OFUSCAR

#define STR_OFUSCAR_1 puts ("\nApplying obfuscation with encryption!\n");
#define STR_OFUSCAR_2 puts("\nJust applying obfuscation!\n");

#define STR_DES_OFUSCAR_1 printf("Size of plain text from vector obfuscated: %d\n", (int) tam);
#define STR_DES_OFUSCAR_2 puts ("\nDecrypting buffer obfuscated\n");
#define STR_DES_OFUSCAR_3 puts("\nInterpreting buffer obfuscated\n");

#define STR_CIFRAR_TAM_1 puts("Encrypting size of vector...");
#define STR_CIFRAR_TAM_2 printf("Size of vector encrypted %d\n", *serializa);


#define STR_DES_CIFRAR_TAM_1 puts("Decrypting size of vector");
#define STR_DES_CIFRAR_TAM_2 printf("Size of plain text of vector%d\n", *plano);
#define STR_DES_CIFRAR_TAM_3 ERROR ("Error while attemp to read the packet...");

#endif

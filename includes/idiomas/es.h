#ifndef SPANISH_H
#define SPANISH_H

/*!\file
 * Mensajes de salida en versión Español
 */

#define STR_GRABAR_BUFFER_ARCHIVO_1   "%s/%d_%d_%d_%d:%d:%d_%s"
#define STR_GRABAR_BUFFER_ARCHIVO_2   "Grabando archivo %s\n"

#define STR_PROCESAR_ARCHIVO_1   "\rSerializando..."
#define STR_PROCESAR_ARCHIVO_2   "%s/%s"
#define STR_PROCESAR_ARCHIVO_3   "\nInformación del VECTOR"
#define STR_PROCESAR_ARCHIVO_4   "Nombre del archivo: |%s| + 1   %d\n"
#define STR_PROCESAR_ARCHIVO_5   "Tamaño del archivo adjunto: %d Bytes\n"
#define STR_PROCESAR_ARCHIVO_6   "Longitud final: %d + %d (int) + %d    "
#define STR_PROCESAR_ARCHIVO_7   "%d Bytes\n"

#define STR_CREAR_DIRECTORIO_1   "\nCreando dir: %s\n"

#define STR_GUARDAR_PAQUETE_1   "Nombre del archivo: %s\n"
#define STR_GUARDAR_PAQUETE_2   "\nTam de archivo:  %d\n"

#define STR_LEER_PAQUETE_1   "LEYENDO SOCKET: %d\n" 

// Header
#define STR_CREAR_CARPETA "\rCreando dir: %s\n"
#define STR_CONTROL_HILOS_1  "\r... Creando hilos"
#define STR_LANZAR_HILOS_1   "\rLanzando hilos...."

// LOGS

#define STR_IMPRIME_TIEMPO_TRANS_1 "Cronometro de tiempo transcurrido: %f (segs) \nen la la linea %d, fichero %s\n"
#define STR_GUARDAR_BITACORA_1 "\rCreando dir: %s\n"
#define STR_GUARDAR_BITACORA_2 "\rGuardando log....                            "
#define STR_DETECTAR_ATAQUE_1 "Posible ataque de contaminación"


// SOCKET

#define STR_RESETEA_SOCKET_1 "Error al inicializar socket: "
#define STR_CONFIGURAR_IP_1 "Error al convertir al convertir IP"

#define STR_PREPARA_SERVIDOR_1 "\rError del proceso bind"
#define STR_PREPARA_SERVIDOR_2 "\rProceso bin correcto\nServidor escuchando..."

#define STR_CONECTAR_1 "\rError del proceso conectar : Esperando intento ..."
#define STR_CONECTAR_2 "\rProceso conectar correcto : Enlazado al servidor."

#define STR_ESCUCHAR_1 "Error del proceso bind: "
#define STR_LEER_1 "%d"
#define STR_LEER_2 "Error en el proceso de lectura: "

#define STR_MOSTRAR_CLIENTE_1 "\nIP entrante %s\nPuerto asignado: %lo\n"
#define STR_ACEPTAR_1 "Esperando a clientes..."
#define STR_ACEPTAR_2 "Error de accept:"
#define STR_INICIAR_SERVIDOR_1 "Iniciando servidor"
#define STR_INICIAR_SERVIDOR_2 "Termina el servidor"
#define STR_DESTRUIR_SOCKET_1 "Liberando buffer"

#define STR_MULTICAST_RECEPTOR_1 "\n\n"
#define STR_MULTICAST_RECEPTOR_2 "\rRecibiendo  %d bytes                                "
#define STR_MULTICAST_RECEPTOR_3 "\rDecodificando %d bytes "
#define STR_MULTICAST_RECEPTOR_4 "Nodo A, recibió : %d\n"
#define STR_MULTICAST_RECEPTOR_5 "Nodo B, recibió : %d\n"

#define STR_MULTICAST_EMISOR_1 "\rCodificando  %d                        "
#define STR_MULTICAST_RECEPTOR_EMISOR_1 "\rRe-enviando = %d bytes, acumulado = %d             "
#define STR_MULTICAST_RECEPTOR_EMISOR_2 "SOCKET LECTURA"

// SEGURIDAD //
// CIFRADO
#define STR_LIBERAR_MANEJADOR_SEGURO_1 puts("\nLiberado el manejador seguro");
#define STR_CONFIGURAR_LLAVE_CIFRADO_1 puts("\nSe ha configurado la llave correctamente.\n");
#define STR_CREAR_MANEJARO_SEGURO_1 fputs ("La biblioteca del proyecto libgcrypt. No puede inicializar\n", stderr);
#define STR_CREAR_MANEJARO_SEGURO_2 puts("Se ha creado el manejador seguro");
#define STR_CIFRAR_BUFFER_1 puts("\nCifrando\n");
#define STR_DES_CIFRAR_BUFFER_1 printf("\nTamaño del buffer cifrado %d\n", (int) tam);

// OFUSCAR

#define STR_OFUSCAR_1 puts ("\nOfuscación con cifrado\n");
#define STR_OFUSCAR_2 puts("\nOfuscacion sin cifrado\n");

#define STR_DES_OFUSCAR_1 printf("Tam de la des-ofuscacion: %d\n", (int) tam);
#define STR_DES_OFUSCAR_2 puts ("\nDecifrando buffer ofuscado\n");
#define STR_DES_OFUSCAR_3 puts("\nDes-ofuscacion sin cifrado\n");

#define STR_CIFRAR_TAM_1 puts("Cifrando el tamaño del vector final");
#define STR_CIFRAR_TAM_2 printf("Tam serializado y cifrado %d\n", *serializa);


#define STR_DES_CIFRAR_TAM_1 puts("Decifrando el tamaño del vector final");
#define STR_DES_CIFRAR_TAM_2 printf("Tam serializado y des-cifrado %d\n", *plano);
#define STR_DES_CIFRAR_TAM_3 ERROR ("Error al obtener tam del paque...");



#endif

#ifndef NETWORK_CODING_H
#define NETWORK_CODING_H

/*!\file
 * Codificación de red, operaciones artimeticas sobre vectores
 */

#include <malloc.h>
#include "network.h"


/* 
 * Codificar  
 * Computa bit xor bit de los flujos A, B,
 */
size_t codificar (char *A,       ///< Vector A no nulo
		  char *B,       ///< Vector B no nulo
		  size_t len_A   ///< |A|
		  );
/*
 * Decodificar
 * Computa bit xor bit de los flujos xor, A
 * xor almacena el resultante
 */
void decodificar (char *, char *, 
                  size_t , size_t );

/// Experimental
void detectar_ataque (char *A, char *XOR);
/// Codifica los vectores inputA e inputB, como resultante el vector output
void codificar2archivos(char *inputA, char *inputB, char *output); 

#endif

#ifndef SOCKET_H
#define SOCKET_H

/*!\file
 * Cabezales para crear SOCKET 's, incluye funciones de comunicación
 */

#include "network.h"


/*!\brief Una instancia permite crear nodos para la trasmisión
 * de datos y archivos
 *
 * Estructura SOCKET usando IPV4, TCP
 * configurar SOCKET invocando nuevo_socket()
 */
typedef struct SOCKET{
	int  unsigned  puerto;         ///< Puerto habilitado por el firewall. Eje. 8080
	int  unsigned  clientes;       ///< No. de Clientes permitidos, default 1
	int  unsigned  *router;        ///< Puertos abiertos Si el nodo será Router
	int  conecta;                  ///< Archivo descriptor
	int  conexion_actual;          ///< Archivo descriptor actual atendido (clientes)
	int  estado_conexion;          ///< Estado de la conexión (return of send())
	char *ip_servidor;             ///< IP para conectar con Servidor (SOCKET cliente)
	char *buffer;                  ///< Nombre del vector a ENVIAR o RECIBIR. Eje. "audio.m4a"
	size_t tam_buffer;             ///< Dimensión del vector
        struct sockaddr_in dir_socket; ///< Objeto sockaddr_in 
        struct sockaddr dir_cliente;   ///< Objeto sockaddr (Info. del cliente)
	char *ruta;                    ///< Ruta para almacenar un adjunto binario 	
	char *etiqueta;                ///< Alias del SOCKET eje. R1.c

}SOCKET;


/**  TCP SOCKET: Creación de un struct SOCKET - Archivos descriptores */ 

SOCKET nuevo_socket(char *IP, unsigned int PORT);  ///< Crea un nuevo objeto SOCKET 
SOCKET resetear_socket(SOCKET);	                   ///< Configura atributosnecesarios de SOCKET

/** Configurar un socket para el inicio de la comunicación */
enum OP_SOCKETS {
	         ENVIAR, ///< El socket envía datos (send)
                 RECIBIR ///< El socket recibe datos (recv)
		 }; 

#define CAST (struct sockaddr *)           ///< Casting a struct sockaddr*
#define TIEMPO_INTENTO  sleep(1)           ///< Tiempo para pausa y Re-Conexión   
 
#define FLAG_SEND MSG_MORE                 ///< Bandera usada multicast*
#define FLAG_RECV MSG_WAITALL              ///< Bandera usada en invo. de funciones send,read
#define FLAG_ROUTER MSG_MORE|MSG_DONTROUTE ///< Bandera usada en invo. de funciones send,read

#define SOCKET_DOMAIN AF_INET              ///< Domino del descriptor SOCKET



/*------------------------------------------------------------------------------------------------ç
 * *
 * 	Operaciones fundamentales de SOCKET
/------------------------------------------------------------------------------------------------*/

void mostrar_cliente(SOCKET *socket);     ///<  Muestra info. sobre un cliente conectado
void destruir_socket(SOCKET *socket);     ///<  Destruye un SOCKET, incluyendo los buffer's 
void conectar(SOCKET *socket);            ///<  Conecta un SOCKET 
void preparar_servidor(SOCKET *socket);   ///<  Prepara los atributos para modo servidor
void escuchar(SOCKET *socket);            ///<  Configura el modo Listen para esperar clientes
void leer(SOCKET *socket);                ///<  Lee información de un socket, archivo descriptor.
void aceptar (SOCKET *socket);            ///<  Valida las condiciones para aceptar una conexión 
void iniciar_servidor(SOCKET *socket);    ///<  Inicia un socket modo Servidor - Transmisor
void iniciar_cliente(SOCKET *socket);     ///<  Inicia un socket modo Cliente - Receptor
size_t mayor_tam (SOCKET *, SOCKET *);    ///<  Compara el tamaño de buffers entre dos SOCKET's
char *extraer_ip (char *argv);            ///<  Devuelve el vector de una dir. IP desde argv[]
unsigned int extraer_puerto (char *argv); ///<  Devuelve un entero

/**
 *  Entornos Multicast, bajo TCP (Simulación Experimental)
 */

/*------------------------------------------------------------------------------------------------
 **	Fuentes y sumideros  - Sources, Sinks
/------------------------------------------------------------------------------------------------*/

/// Simulación multicast, emisores A,B (RECIBIR - ENVIAR)
void multicast (SOCKET *nodoA, SOCKET *nodoB); 
/// Simulación multicast, receptores A,B (RECIBIR) 
void multicast_receptor(SOCKET *nodoA, SOCKET *nodoB); //   Leyendo de nodoA y nodoB

/*------------------------------------------------------------------------------------------------
 * 	Nodos intermedios - Intedmediates
/-----------------------------------------------------------------------------------------------*/
/// Simulación multicast, receptores A,B, emisor (RECIBIR-ENVIAR)
void multicast_emisor(SOCKET *emisor, SOCKET *nodoA, SOCKET *nodoB); // un SOCKET emite A && B

/*------------------------------------------------------------------------------------------------
 * 	Nodos Re-transmisores  - Relays
/-----------------------------------------------------------------------------------------------*/
//   Un receptor retransmite a nodos A y B
void multicast_receptor_emisor(SOCKET *receptor, SOCKET *emisorA, SOCKET *emisorB);

/*------------------------------------------------------------------------------------------------
 * 	Operaciones para arquitectura IDA - Dispersal Alghoritm Arquitecture
/-----------------------------------------------------------------------------------------------*/ 

/// Crea un nodo etiqueta T
SOCKET define_receptorT(int mod);
/// Crea un nodo receptor  disperso	      
SOCKET receptor_disperso(int mod);             
SOCKET *sockets_dispersales (size_t limit);     ///< Crea un vector de descriptores n=limit
void limpiar_dispersales (SOCKET *nodo);        ///< Limpia la mem. de un SOCKET*
void trasmitir5dispersos(char *ip_dispersal,int unsigned *puertos);       ///< Crea, transmite 5 descriptores (0...4) MultiThread
void trasmitir_archivo (SOCKET *emisor);	///< Un  SOCKET* se conecta  y recibe un buffer
void recibir_archivo (SOCKET *emisor);		///< Un  SOCKET* se conecta  y recibe dos buffer's
void recibir2archivos (SOCKET *, SOCKET *);     ///< Dos SOCKET* se conectan y recibe cada uno un buffer
void router(SOCKET *emisor);			///< Un  SOCKET* relay puede almacenar un buffer
void chat(SOCKET *, int);			///< Testing chat(SOCKET*, ENVIAR|RECIBIR) 
/*------------------------------------------------------------------------------------------------
 *  Funciones P2P para nodos receptores R*
/-----------------------------------------------------------------------------------------------*/ 
void p2p_codificador(SOCKET *a, 	      
		     SOCKET *b,  
		     SOCKET *c);                // Funciones de un nodo P2P codificador        
void p2p(SOCKET *a,SOCKET *b,SOCKET *c);        // Funciones de un nodo P2P
 
/*------------------------------------------------------------------------------------------------
 *
/-----------------------------------------------------------------------------------------------*/ 



#endif

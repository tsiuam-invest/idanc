#ifndef NETWORK_H
#define NETWORK_H
#define CODIFICAR
/*!\file
 * Cabezales para la comunicación de redes.
 * Berkeley Sockets
 */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include "config.h"
#include "socket.h"
#include "network_coding.h"

#define LOCAL_HOST "127.0.0.1"                               ///< Default IP Address
#define PUERTO_PREDETERMINADO 1500

#endif

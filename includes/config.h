#ifndef CONFIG_H
#define CONFIG_H

/*!\file
 *  Configuración del simulador:
 *
 *  - Rutas de salida y entrada.
 *  - Dimensiones de vectores.
 *  - Direcciones IP y puertos definidos para los nodos.
 *  - Activación de registros tipo log. 
 *
*/

/* 
//#define PAQUETE_OFUSCADO
//#define PAQUETE_CIFRADO
//#define LIMIT_SIZE (BYTES*BYTES)		      ///< Límite de vector fuente, 1 Mb
*/

/*--------------------------------------------------------------------------------------
 * 	Rutas
/--------------------------------------------------------------------------------------*/
#define FILE_NAME "archivo.data"                           ///< Nombre del vector fuente
#define INPUT_PATH "input"                        ///< Ruta de  vectores fuentes
#define OUTPUT_PATH "output"                      ///< Ruta de vectores dispersados
#define CREAR_LOG "logs"                          ///< Ruta de archivos tipo logs

/*-------------------------------------------------------------------------------------
 * 	IDIOMA  (en, es)
/-------------------------------------------------------------------------------------*/
#include "idiomas/es.h"   

/*-------------------------------------------------------------------------------------
 * 	Direcciones IP
/-------------------------------------------------------------------------------------*/

#define LOCAL_HOST "127.0.0.1"                               ///< Default IP Address
/*-------------------------------------------------------------- Topologías mariposa */

#define IP_DISPERSAL "0.0.0.0"
#define PUERTO_D1 512
#define PUERTO_D2 612
#define PUERTO_D3 712
#define PUERTO_D4 812
#define PUERTO_D5 912

#define IP_N1 "0.0.0.0"
#define IP_N2 "0.0.0.0"
#define IP_N3 "0.0.0.0"
#define IP_N4 "0.0.0.0"
#define IP_N5 "0.0.0.0"
#define IP_C1 "0.0.0.0"
#define IP_C2 "0.0.0.0"
#define IP_T1 "0.0.0.0"
#define IP_T2 "0.0.0.0"
#define IP_R1 "0.0.0.0"
#define IP_R2 "0.0.0.0"
#define IP_R3 "0.0.0.0"
#define IP_R4 "0.0.0.0"
#define IP_R5 "0.0.0.0"

/*-------- ----------------------------------------------------- Apertura de puertos */
#define PUERTO_A 2000
#define PUERTO_B 3000
#define PUERTO_C 4000
#define PUERTO_D 5000
#define PUERTO_E 6000

#define PUERTO_PREDETERMINADO 1500

/*-------- -----------------------------------------------------------------Cabezales */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h> 
#include <fcntl.h>
#include <sys/stat.h> 
/*----------------------------Bibliotecas del proyecto-----------------*/
#include "utilidades/tiempo.h"                        
#include "utilidades/util.h"                        
#include "red/network.h"			     
#include "ida/ida.h"
#include "debug/debug.h"
#include "seguridad/seguridad.h" 

#endif

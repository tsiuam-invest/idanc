#ifndef UTIL_H
#define UTIL_H
/*!\file 
 * Este archivo incluye TODOS los cabezales
 *
 * Cabezal principal
 *
 * 
 */
#include "config.h"
                    
/*!\brief  
 * Control de hilos para los métodos de la biblioteca pthread.h 
 */
typedef struct CONTROL_HILOS {
	pthread_t hilo;        
	struct CONTROL_HILOS *sig; ///< Var. de control de enlace.
} *CONTROL_HILOS;


/// Crea un hilo, ejecutando algún proceso 
void crear_hilo (void *ptr_funct(), ///< Puntero a función 
		 void *ptr_para);  ///<  Puntero a variable

/// Indica al sistema el siguiento de cada hilo en una lista tipo CONTROL_HILOS  (C_I)
void lanzar_hilos (void);

/// Define un objeto como hilo
#define HILO pthread_t
/// Define un proceso como MUTEX
#define MUTEX pthread_mutex_t

/// Crea un directorio con el path nombre
void crear_directorio(char *directorio);
/// Concatena dos vectores: a + b
char *concat(char *a, char *b);
/// Fragmenta un vector acorde a un token y a un limite
char *xtoken(char *buffer, char *token, size_t limite);
size_t  obtener_tam_archivo(char *ruta);


/// Muestra un error, tratando de indicar la línea y el fichero
#define ERROR(cadena)(\
{\
  perror(cadena);\
  printf("Más info: Linea de código %d, Programa %s", __LINE__, __FILE__);\
  abort();\
})
/// Recibe una expresión lógica, evalua la salida
#define EXIT_IF(expr)(\
{\
  if(expr){\
  	char *e = strerror(errno);\
	fprintf(stderr,"\a");\
	fprintf(stderr,RED("\fEXIT_IF(%s) in "), LAMBDA(expr));\
	fprintf(stderr,"%s |%s : %d| %s",__FILE__,__func__, __LINE__, e);\
	fprintf(stderr,"\n");\
  	exit(EXIT_FAILURE);\
  }\
})

/// Convierte un vector a un int unsigned
#define TO_UINT(cadena)(\
{\
  assert(cadena);\
  strtoul(cadena, '\0', 10);\
})
/// Define un bool propio
#define bool int
/// Valores permitidos para funciones tipo bool
enum __bool_ {
      False, ///< 0 
      True,  ///< 1
      };
///  Convierte un macro var en str
#define LAMBDA(x) #x

/// Colorea cadena
#define RED(S) "\x1B[31m"S"\x1B[0m"
#define BOLD(S) "\x1B[32m"S"\x1B[0m"
#define YELLOW(S) "\x1B[33m"S"\x1B[0m"
#define OK BOLD(": OK")

#define BYTES 1024

#endif


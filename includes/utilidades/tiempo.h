#ifndef TIEMPO_H
#define TIEMPO_H
/*!\file
 * Cálculo de tiempos de procesamiento, reloj del la CPU.
 */

/*!\brief Una instancia permite crear un registro tipo log 
 * 
 *  - Se utiliza para cronometrizar los tiempos del CPU.
 */
#include "config.h"

typedef struct BITACORA {
	char *nombre_nodo;   ///< Nombre del nodo a registrar
	size_t tam;          ///< Logitud de vectores procesados (bytes) por nombre_nodo
	double crono;        ///< Tiempo transcurrido desde la llamada CRONOMETRO()
}BITACORA;

/// Var. global para cronometrizar una BITACORA
extern BITACORA cronometro; 

typedef struct CLOCK{
	int cuenta;
	struct timespec inicio;
	struct timespec fin;
}CLOCK;


#define NANO_SEGUNDOS 1000000000 

/// Macro invocadora de calc_tiempo_trans 
//#define CRONOMETRO(); calc_tiempo_trans();
#define CRONOMETRO(); cronometro_relog();
/// Macro invocadora de imprime_tiempo_trans, LINE-> linea de codigo, FILE -> programa 
#define MUESTRA_CRONOMETRO(); imprime_tiempo_trans (__LINE__, __FILE__);
/// Macro invocadora de guardar_bitacora 
#define GUARDA_BITACORA(); guardar_bitacora(__FILE__ );
/// Muestra el tiempo transcurrido para un fichero 
void imprime_tiempo_trans (int linea, char *fichero);
/// Usando el reloj de una CPU, calcula el tiempo transcurrido 
void calc_tiempo_trans (void);
/// Guarda una bitacora para un modulo 
void guardar_bitacora(char *nombre);

void cronometro_relog();

#endif

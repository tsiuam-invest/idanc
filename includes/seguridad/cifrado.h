#ifndef CIPHER_H
#define CIPHER_H

#include "config.h"
/**
 * Uso de biblioteca Gcrypt - GnuPG *
 * Consultar Doc. en: 
 * https://www.gnupg.org/documentation/manuals/gcrypt/
 **/
#include <gcrypt.h>

/* Conf. del Cifrado  */
#define AES GCRY_CIPHER_AES192    /// Longitud de Cifrado y ciclaje
#define MODE GCRY_CIPHER_MODE_GCM /// Modo. de cifrado de bloques (ciclos).
#define FLAG 0                    /// Bandera optativa
#define LONGITUD_K 16             /// Longitud de Vector K
#define PASSWORD NULL             /// Pasword

void cifrar_buffer (char *buffer, char *salida, size_t tam);        /// Cifra un buffer de tam. específico
void des_cifrar_buffer (char *entrada, char *salida, size_t tam);   /// Descifra un buffer de tam. específico
void cifrar_archivo(char *input, char *output);
void des_cifrar_archivo(char *input, char *output);
void ofuscar_archivo(char *input, char *output);
void des_ofuscar_archivo(char *input, char *output);
#endif

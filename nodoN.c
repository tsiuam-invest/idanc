/*!\file 
 * Nodo receptor etiqueta R3. 
 * uso: ./exe
 */

#include "includes/config.h"
int main(int argc, char  *argv[])
{ 
   dauto();  
   SOCKET receptor;
   switch(*argv[1]) { 
	  	case 'A': receptor = nuevo_socket(IP_DISPERSAL, PUERTO_A);
			  break;
	  	case 'B': receptor = nuevo_socket(IP_DISPERSAL, PUERTO_B);
			  break;
	  	case 'C': receptor = nuevo_socket(IP_DISPERSAL, PUERTO_C);
			  break;
	  	case 'D': receptor = nuevo_socket(IP_DISPERSAL, PUERTO_D);
			  break;
	  	case 'E': receptor = nuevo_socket(IP_DISPERSAL, PUERTO_E);
			  break;
		default:  exit(-1);
			  }
	
   int unsigned puertos[5] = {PUERTO_A, PUERTO_B, PUERTO_C, PUERTO_D, PUERTO_E};
   receptor.router = puertos;
   receptor.clientes = 5;
   router(&receptor);


   return 0;
}

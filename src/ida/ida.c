#include "ida.h"
/**
 *   ida_c es IDA (Information dispersal algorithm).
 *     
 *   Autor: Prof.Marcelín Jiménez Ricardo
 *   Modificado por Ortega Vallejo Raúl Antonio.
 *     
 *   Proyecto de Investigación: 
 *   Prof.López Fuentes Franciso de Asís.
 *     
 *   UAM Iztapalapa, UAM Cuajimalpa.
 *   Más info favor de leer docu. 
 */

/*
 *   Privados de IDA
 *   Las constantes no deben modificarse
*/
static const int n_dispersales = 5;
static const int n_umbral      = 3;
static const int PRIMITIVO     = 369;
static const char ida_matrix[5][3] = {
                     {'1', '3', '2'} ,
		     {'1', '1', '1'} ,
		     {'2', '3', '1'} ,
		     {'2', '2', '3'} ,
		     {'2', '3', '3'} ,
		   };
/**
 * Atributos de un operador de IDA
 */
struct ida_op{ 
  unsigned int orden;
  unsigned char d[3];
  unsigned char g[3][3];
  unsigned char EXP[256];
  unsigned char LOG[256];
  unsigned char aRec[3][3];
  unsigned char a[5][3]; 
};
/**
 * Crea un operador ida_op *
 */
static struct ida_op*
set_op(void)
{ 
  struct ida_op *op = (struct ida_op*)malloc(sizeof(struct ida_op));
  if(!op) 
     return NULL;
  memcpy(op->a,ida_matrix,sizeof(op->a));
  return op;
}
/**
 * Destruir un operador ida_op *
 */
static void
destroy_op(struct ida_op **op)
{
  if(op && *op)
     free(*op);
  *op = NULL;
}

/*
 * Recorre 'v' a la derecha y determina la ultima vez que el LSb es 1 o 0.
 */
static int 
grado(int v)
{  
   unsigned int i, j;
   unsigned int l, w;

   l = 0;
   w = 1;
   j = 8 * sizeof(int);
   for (i = 1; i < j; i++) {  
      l = (v & w) ? i: l;
      v = (v >> 1);   
   }

   return l;
}

/*  
 *  Suma en modulo 2 bit por bit
 */
static unsigned int 
suma(unsigned int a,unsigned int b)
{
    /*std::cout << " suma " << a << "+" <<  b; */
   return(a ^ b);
}

/* 
 * Multiplicacion por medio de LOGaritmos
 */
static unsigned int 
mult(unsigned int a,unsigned int b, struct ida_op *op)
{
   /* std::cout << " mult " << a << "x" << b;  */
   if ((a!=0) && (b !=0))
      return(op->EXP[(op->LOG[a] + op->LOG[b]) % op->orden]);
   else
      return(0);
}

/* 
 * División por medio de LOGaritmos
 */
static unsigned int 
division(unsigned int a,unsigned int b,struct ida_op *op)
{  if (b == 0)
      return(op->orden + 1);
   else if (a == 0)
      return(0);
   else
      return(op->EXP[(op->LOG[a] + op->orden - op->LOG[b]) % op->orden]);
}


/*
 * Invierte una matriz(aRec) cuadrada de 3x3 
 */
static void 
invierteM(struct ida_op *op)
{
   int i,j;
   unsigned int d;

   d=0;
   for (j=0; j<3; j++) {
      d=suma(d,mult(op->aRec[0][j],
                    suma(mult(op->aRec[1][(j+1)%3],op->aRec[2][(j+2)%3],op),
                         mult(op->aRec[1][(j+2)%3],op->aRec[2][(j+1)%3],op)),
		    op)
             );
   }
   
   /* std::cout << "d = " << d << endl; */
   for (i=0; i<3; i++) {
      for (j=0; j<3; j++) {
          op->g[j][i]=division(suma(
	                       mult(op->aRec[(i+1)%3][(j+1)%3],op->aRec[(i+2)%3][(j+2)%3],op),
                               mult(op->aRec[(i+1)%3][(j+2)%3],op->aRec[(i+2)%3][(j+1)%3],op)),
			       d,op);
      }
   }
}

/*
 * Genera el campo finito a partir de su polinomio primitivo. GF
 */
static void 
generaGF(struct ida_op *op)
{  
   memset(op->EXP,0,255);
   memset(op->LOG,0,255);
   unsigned int      dg;        /* cociente parcial ("toca a 0 o 1?")      */
   unsigned int      rx;        /* residuo                                 */
   unsigned int      gx;        /* polinomio primitivo                     */
   unsigned int     ngx;        /* g(x), desplazamiento a la izq.          */
   unsigned int       i;

   int leng1        = 0;        /* grado de g(x)                           */
   int leng2        = 0;        /* grado de r(x)                           */

   gx = PRIMITIVO;
   leng1 = grado(gx);
   op->orden = 1 << (leng1 - 1);
   op->orden = op->orden - 1;
   rx = 1;
   for (i = 0; i < op->orden; i++) {  
      leng2 = grado(rx);
      dg    = 1;
      dg  = (leng2 - leng1 >= 0) ? dg << (leng2 - 1): 0;
      ngx = (leng2 - leng1 >= 0) ? gx << (leng2 - leng1): 0;

      while (ngx >= gx){   
        /* si "toca" a 1 */
       if( dg == (rx & dg))  
	       rx=rx^ngx;
        dg=(dg>>1);
        ngx=(ngx>>1);
      };

      op->EXP[i] = rx;  
      op->LOG[rx] =i;  
      /* std::cout << "LOG (" << rx << ")=  " << i << "\n"; */
      rx = rx<<1;
   };
}

/*
 *  Cierra un archivo 
 */
static void 
cerrarArchivoMem (FILE **archivo) 
{
       fclose(*archivo);
       *archivo = NULL;
}
/*
 * Graba el último byte: size % 3,
 * cierra dispersal
 */
static void 
cerrarDispersal (FILE **dis, size_t tamFinal)
{
       rewind(*dis); 
       putc(tamFinal % 3, *dis);
       cerrarArchivoMem(dis);
}

/*
 * Lectura de 4 bytes de f.
 * insertando en matriz aRec
 * devuelve primer byte
 */
static char
obten_dispersal(FILE *f, size_t index, struct ida_op *op)
{
       char r = getc(f);
       int j = 0;		
       for (; j < 3; j++) 
	       op->aRec[index][j] = getc(f);
       return r;	       
}
/* 
 * Graba 4 bytes en f
 */
static void 
graba_dispersal(FILE *f, size_t index, struct ida_op *op)
{	
       /* Graba primer renglon ' ' (pude ser 0)  */
       putc(' ', f);
       int j = 0;
       for (; j < 3; j++) 
	     putc(op->a[index][j], f);
}

static int
iniciaRecuperacion(FILE **input,FILE *out,int r,struct ida_op *op)
{
   int i,j;
   int size = 0;
   int primera = 1;
   unsigned int t = 0;
   unsigned int *aux[3];
   char c = 0;
   char b[3]; 
   char c3[12];
   memset(c3,0,12);
   memset(b,0,3); 
   generaGF(op);
   invierteM(op); 

   for (j = 0; j < 3; j++)
        aux[j] = (unsigned int *) &c3[4*j];

   do{ 
      ida_printf("Espere\tprocesando %d bytes \r",size);
      for (j = 0; j < 3; j++) {
	    c = getc(input[j]);
            if (!feof(input[j])) {  
	         c3[4 * j] = c;
                 op->d[j] = (*aux[j]);
             }
      }

      if ((!feof(input[0]))&&(!primera)) {  
	  for (i = r; i<3; i++, size++)
               putc(b[i], out);

      }
      if (!feof(input[0])) {  
	 for(i = 0; i < 3; i++) {  
             for(t = j = t = 0; j < 3; j++)
                 t = suma(t, mult(op->g[i][j], op->d[j], op));
             b[i]=t;
          }

         for (i = 0; i < r; i++, size++)
              putc(b[i], out);
      }
      primera = 0;
      }while(!feof(input[0]));

   /* Cerramos dispersados ... */
   cerrarArchivoMem(&input[0]);
   cerrarArchivoMem(&input[1]);
   cerrarArchivoMem(&input[2]);
   return size;
}

static int
iniciaDispercion(FILE **archivo,FILE *in,struct ida_op *op)
{
   int size = 0;
   int i,j,t;
   unsigned char b[3];
   generaGF(op);
   while(!feof(in)) {
	 ida_printf("Espere\tprocesando %d bytes \r",size);
         memset(b, 0, 3);	   
         j = 0;
        do{ 
          b[j++] = getc(in);
          if(!feof(in)) 
              size++;
          }while((!feof(in)) && (j < 3));

        if (j > 0) {
	    for (i = 0; i < 5; i++) {  
                 for(t = j = 0; j < 3; j++)
                     t = suma(t, mult(op->a[i][j],b[j],op));
	    putc(t, archivo[i]);
            }
        }
   }
   /* Grabamos y cerramos dispersados ... */
   for(i = 0; i < 5; i++)
       cerrarDispersal(&archivo[i],size);

   return size;
}

static void
clean(FILE **abierto,struct ida_op **op)
{
   cerrarArchivoMem(abierto);
   destroy_op(op);
   *abierto = NULL;
   *op = NULL;
   ida_printf("\tTermina operacion IDA.                                      \r");
}

int
ida_dispersa(char *file, char *dir_disp, char *ext)              
{  
   ida_return_if(!file || !dir_disp || !ext, "null values");
   struct ida_op *op = set_op();
   ida_return_if(!op,"Error");
   FILE *input = fopen(file, "rb");
   ida_return_if(!input, file);
   char dispersal[256] = "\0";
   int i;
   int lectura  = 0;
   /* 
    * Los n + 1 primeros bytes del archivo son: |F|mod(n), el exceso del archivo
    * fuente y los n bytes del renglon de la matriz que da origen al disperso
    */
   FILE *output[5] = {NULL};
   for (i = 0; i < 5; i++) {
	snprintf(dispersal,255,"%s%d%s",dir_disp,i,ext);
	if(!(output[i] = fopen(dispersal,"wb"))) {
	   clean(&input,&op);
	   ida_return_if(!output[i],dispersal);
	}
        graba_dispersal(output[i],i,op);
	ida_printf("\tCreado %s\n",dispersal);
   }
   lectura = iniciaDispercion(output,input,op);
   clean(&input,&op);
   return lectura;
}
/* 
 * Reconstruye el archivo original a partir de sus pedazos
 */
int 
ida_recupera(char *file, char *dir_disp, char *ext)              
{
   ida_return_if(!file || !dir_disp || !ext, "null values");
   struct ida_op *op = set_op();
   ida_return_if(!op,"Error");
   FILE *output = fopen(file, "wb");
   ida_return_if(!output,file);

   int i = 0;
   int lectura = 0;
   char renglon = 0;
   /**
    *  Lectura de los dispersales en dir_disp,
    *  sólo se requiere n_umbral de archivos input;
    */
   FILE *input[3] = {NULL};
   char dispersal[256] =  "\0";
   for (; i < n_umbral && lectura < n_dispersales; lectura++) {
	snprintf(dispersal,255,"%s%d%s",dir_disp,lectura,ext);
	if((input[i] = fopen(dispersal,"rb"))) {
	    renglon = obten_dispersal(input[i],i,op); 
	    i++;
	}
      }
   if(i < n_umbral) {
      clean(&output,&op);
      ida_return_if(i < n_umbral,"Insuficientes dispersales");
   }
   lectura = iniciaRecuperacion(input,output,renglon,op);
   clean(&output,&op);
   return lectura;
}

int
ida_tam(int size) 
{	
   return (size > 0 ? size / n_umbral + n_dispersales : 0);
}


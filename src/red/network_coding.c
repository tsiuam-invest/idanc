#include "network_coding.h"

/**
   Proceso de codificación:
   Optimización del uso de memo 1Kb = 1024
   Asume dos vectorer no nulos, con longitud  BYTES >= len >=  0
 */

size_t 
codificar(char *A,char *B,size_t len_A)
{
	int i;
	//size_t tam = (len_A < len_B) ? len_B : len_A;
	size_t tam = len_A;
	for (i = 0; i < tam  ; i++) 
		    *(A++) ^= *(B++);
	return tam;
}


/** 
 * Asume que los vectores  no son Nulos,
 * con len >= 0
 * Codifica XOR ^ A
 */
void 
decodificar (char *A,char *XOR,size_t len_A, size_t len_XOR) 
{	
	int i;
	for (i = 0; i < len_XOR && i < len_A; i++)
	 	*(XOR++) ^= *(A++);
}

/* 
 *  detectar_ataque(char *a, char *xor); 
 *  ¡Experimental!
 *  Intenta evitar un ataque contaminación de flujos,
 *  esta función debe usarse en nodos intemediarios
 *  o re-transmisores. 
 */
void 
detectar_ataque (char *A, char *XOR) {
	char evidencia =  *A ^ *XOR;
	if ((evidencia ^ *XOR) != *A) {
		MUESTRA_CRONOMETRO();
		ERROR (STR_DETECTAR_ATAQUE_1); 
          }
}

/**
 * La función lee toma los archivos del sistema,
 * provenientes de los vectores (nombres de archivos A, B),
 * codificando en un nuevo vector (archivo) Y 
 */
void
codificar2archivos(char *A, char *B, char *Y)
{
	assert(A && B && Y);
	FILE *a = fopen(A, "rb");
	FILE *b = fopen(B, "rb");
	FILE *y = fopen(Y, "wb");
	assert(a && b && y);
	char xor = '\0';
	while(!feof(a) && !feof(b)) {
		xor = getc(a);
		if (feof(a) || feof(b)) break;
		xor ^= getc(b);
		putc(xor, y);
	}
	fclose(y);
	fclose(a);
	fclose(b);
	a = b = y = NULL;
}


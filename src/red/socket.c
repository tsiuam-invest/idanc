/*****************************************************************************************
*		           CODIFICACIÓN DE RED                                     *   
*		           Raúl Antonio Ortega Vallejo                        
\****************************************************************************************/

#include "socket.h"

/**
 *  Recibe un buffer, en formato IP eje: "127.0.0.1:500"
 *
 *  Se regresa un corte  0  hasta el caracter ':'
 */
char *
extraer_ip (char *buffer) 
{	
	return xtoken(buffer,":", 0);
}

/**
 * Recibe un buffer, en formato IP eje: "127.0.0.1:500"
 * Se regresa un corte a 1 salto del caracter ':' 
 */
unsigned int
extraer_puerto (char *buffer) 
{
	return TO_UINT(xtoken(buffer,":", 1));
}


/*
 * Reseteamos la conf. a predeterminado de un socket
 *
 * Permite reutilizar atributos:
 * - SOCKET.ip_servidor
 * - SOCKET.puerto
 */
SOCKET 
resetear_socket (SOCKET nuevo) 
{	
	nuevo.conecta = socket (SOCKET_DOMAIN,SOCK_STREAM, 0);
	if (nuevo.conecta < 0) 
		ERROR(STR_RESETEA_SOCKET_1);
	memset (&nuevo.dir_socket, '0',sizeof (nuevo.dir_socket));
	nuevo.dir_socket.sin_family = AF_INET;
	nuevo.conexion_actual = 0;
	return nuevo;
}
/*
 * nuevo_socket(char *);
 * Crea un SOCKET con configuraciones necesarias
 * en base a resetear_socket(SOCKET)
 */
SOCKET 
nuevo_socket(char *ip, unsigned int port) 
{		
	SOCKET nuevo = resetear_socket (nuevo);
	nuevo.ip_servidor = (ip ? ip : LOCAL_HOST);
	nuevo.puerto = (port ? port  : PUERTO_PREDETERMINADO);
	/* Valores por default */
	nuevo.clientes = 1; 
	nuevo.buffer = NULL;
	nuevo.router = NULL; 
	dpoint();
	return nuevo;
}

/* 
 * configurar_ip(SOCKET *, int)
 * Configura una dir. IP acorde al peer.
 */
static void 
configurar_ip (SOCKET *socket, int accion)
{
        assert(socket);
	switch (accion) {
		case ENVIAR :
			socket->dir_socket.sin_addr.s_addr = htonl(INADDR_ANY);
			break;
		case RECIBIR:
			if (inet_pton(AF_INET, 
				       socket->ip_servidor,
				       &socket->dir_socket.sin_addr) <= 0) { 
				ERROR (STR_CONFIGURAR_IP_1);
			     }
			break;
		}
	// Configuramos el puerto, convirtiendo a formato de orden de bytes
	socket->dir_socket.sin_port = htons(socket->puerto);
}

/* 
 * preparar_servidor (SOCKET *);
 * Prepara el socket como Servidor,
 * bind-listen
 */
void
preparar_servidor(SOCKET *socket)
{	
     assert(socket);
     configurar_ip(socket,ENVIAR);
     int opcion = 1;
     while (1)  {
	     
	     if (bind(socket->conecta,  
		      CAST &socket->dir_socket, 
		      sizeof(socket->dir_socket)) < 0 ) {
		     
		     perror (STR_PREPARA_SERVIDOR_1);
		     TIEMPO_INTENTO;
	     }

	     else if(setsockopt(socket->conecta,SOL_SOCKET,
		                (SO_REUSEADDR),
				(char *)&opcion, sizeof(opcion)) >= 0) {

		     listen(socket->conecta, socket->clientes); 
		     printf(STR_PREPARA_SERVIDOR_2);
		     break;
	     }
	     fflush(stdout);
     }
}

/* conectar(SOCKET *)
 * conecta un SOCKET* con la conf. de los atributos de 
 * struct SOCKET
 */
void 
conectar (SOCKET *socket) 
{
    assert(socket);
    configurar_ip (socket, RECIBIR);
    while (connect(socket->conecta,
		   CAST &socket->dir_socket, 
		   sizeof(socket->dir_socket)) < 0) {
		 printf(STR_CONECTAR_1);
		 fflush(stdout);
		 TIEMPO_INTENTO;
	 } 
     //listen(socket->conecta, socket->clientes);  // Esperando conexiones también
     printf(STR_CONECTAR_2);
     mostrar_cliente(socket);
     fflush(stdout);
     dpoint();
}

/*
 * escuchar(SOCKET *)
 * Prepara un SOCKET* en modo servidor
 * Procesos:
 * bind-listen
 */
void
escuchar (SOCKET *socket)
{
    assert(socket);
     configurar_ip (socket, ENVIAR);
     if ( bind ( socket->conecta,  
           CAST &socket->dir_socket, 
	   sizeof(socket->dir_socket)) < 0 ) {
	     ERROR (STR_ESCUCHAR_1);
     }
     listen(socket->conecta, socket->clientes);
     dpoint();
}


/* leer(SOCKET *)
 * Importante: Método inseguro.
 * Lectura de un buffer con longitud desconocida
 * config en nuevo_socket
*/
void
leer (SOCKET *socket)
{
    assert(socket);
    CRONOMETRO(); // Cronometramos
    socket->buffer = malloc(BYTES);
    assert (socket->buffer);
    int lectura = 0;
    int acumulado = 0; // Acumulado de Bytes recibidos
    do {
	lectura = recv(socket->conecta, socket->buffer + acumulado, BYTES, FLAG_RECV);
	acumulado += lectura;
	socket->buffer  = realloc (socket->buffer, acumulado + BYTES);
	//printf(STR_LEER_1, acumulado);
      }while(lectura > 0);
    
    // Ocurrio un error en el proceso de lectura 
    if (lectura < 0)  {
	    ERROR (STR_LEER_2);
	    destruir_socket(socket);
	    }

    CRONOMETRO(); 
    // Tamaño de lectura del Buffer 
    socket->tam_buffer = acumulado;
    dpoint();
}

/* mostrar_cliente(SOCKET *)
 * asumiendo que SOCKET* es servidor
 * muestra info. relativa al cliente:
 * IP,Puerto.
 */
void
mostrar_cliente (SOCKET *socket) 
{
    	assert(socket);
    	char *ip_cliente  = NULL;
	long unsigned puerto_cliente;
	ip_cliente = inet_ntoa(socket->dir_socket.sin_addr);
	//puerto_cliente = ntohs(socket->dir_socket.sin_port);
	puerto_cliente = socket->puerto;
	// Información delpeer  conectado
	printf(STR_MOSTRAR_CLIENTE_1,ip_cliente,puerto_cliente);
        dpoint();
}

void
aceptar (SOCKET *socket) 
{
    	assert(socket);
    	int unsigned tam_cliente = sizeof(socket->dir_socket);
	puts(STR_ACEPTAR_1);
	do {
 	   socket->conexion_actual = accept( socket->conecta, 
	   		            (struct sockaddr *) &socket->dir_socket, 
				     &tam_cliente); 
	    if (socket->conexion_actual < 0) {
	 	   ERROR (STR_ACEPTAR_2);
	    }
	  }while (!socket->conexion_actual);
	mostrar_cliente(socket);
        dpoint();
}

void
iniciar_servidor (SOCKET *socket) 
{
    assert(socket);
    int clientes = 0;
    /*  
       EXPERIMENTAL:
       obtenemos el tam, del area de momoria, usada para el buffer 
    */
    puts(STR_INICIAR_SERVIDOR_1);
    preparar_servidor(socket);
    CRONOMETRO(); 
    int *conexion = &socket->conexion_actual;
    // int lectura = 0;
    while (clientes < socket->clientes){
	        aceptar(socket);
		mostrar_cliente(socket);
		/* Modo por bloques
		 * while (lectura < socket->tam_buffer)
		     lectura += send(*conexion, socket->buffer + lecturaa, BYTES, MSG_CONFIRM); 
		     */
		send(*conexion, socket->buffer, socket->tam_buffer, FLAG_SEND);
		close(*conexion);
		clientes++;
     }

    CRONOMETRO(); 

    puts (STR_INICIAR_SERVIDOR_2);
    dpoint();
}

void
iniciar_cliente(SOCKET *socket) {
     assert (socket);
     conectar (socket); // Conectamos al server
     leer (socket);  // Recibimos buffer
     dpoint();
}


/**
 * Destruye attibutos de un SOCKET *
 * como  SOCKET.buffer
 */
void 
destruir_socket(SOCKET *socket)
{	
	assert(socket);
	if (socket->buffer && *(socket->buffer)) {
		puts(STR_DESTRUIR_SOCKET_1);
		free(socket->buffer);
		socket->buffer = NULL;
	   }

        dpoint();
}


/**
 * Evaluación de tamaño de los Buffers 
 * A & B
 */
size_t 
mayor_tam (SOCKET *a, SOCKET *b)
{	
	assert(a && b);
	if (a->tam_buffer > b->tam_buffer) 
		return a->tam_buffer;
	else 
		return b->tam_buffer;
}

void 
multicast_receptor (SOCKET *nodoA, SOCKET *nodoB) 
{
    assert(nodoA && nodoB);
    conectar (nodoA);
    conectar (nodoB);

    int lecturaA = 1; 
    int lecturaB = 1; 
    int total =  0;
    size_t acumuladoA =  0;
    size_t acumuladoB =  0;
    nodoA->buffer = malloc(BYTES);
    nodoB->buffer = malloc(BYTES);
    puts(STR_MULTICAST_RECEPTOR_1);
	
    CRONOMETRO();
    while(lecturaA > 0 || lecturaB > 0)  {
	    if (lecturaA > 0) {
	    	lecturaA = recv(nodoA->conecta, nodoA->buffer + acumuladoA, BYTES, FLAG_RECV);
    		acumuladoA += lecturaA;
		nodoA->buffer = realloc (nodoA->buffer, acumuladoA + BYTES);
	    }
	    if (lecturaB > 0) {
	    	lecturaB = recv(nodoB->conecta, nodoB->buffer + acumuladoB, BYTES, FLAG_RECV );
    		acumuladoB += lecturaB;
		nodoB->buffer = realloc (nodoB->buffer, acumuladoB + BYTES);
	    }
	    total += lecturaA + lecturaB;
	    printf(STR_MULTICAST_RECEPTOR_2, total);
	    fflush(stdout);
	 }
    CRONOMETRO();
    printf(STR_MULTICAST_RECEPTOR_3, total);
    decodificar(nodoA->buffer, nodoB->buffer, acumuladoA, acumuladoB);

    printf(STR_MULTICAST_RECEPTOR_4,  (int)acumuladoA);
    printf(STR_MULTICAST_RECEPTOR_5,  (int)acumuladoB);
    nodoA->tam_buffer = acumuladoA;
    nodoB->tam_buffer = acumuladoB;
    dpoint();
}

void
multicast_emisor (SOCKET *emisor, SOCKET *nodoA, SOCKET *nodoB) 
{
    assert(emisor && nodoA && nodoB);
    preparar_servidor(emisor);
    conectar(nodoA);
    conectar(nodoB);
    aceptar (emisor);
    
    int lecturaA = 0; 
    int lecturaB = 0; 
    size_t acumulado = 0;
    char A[BYTES];
    char B[BYTES];
    // Inicializamos....	   
    memset(A, 0, BYTES);
    memset(B, 0, BYTES);
    CRONOMETRO();
    do {  
	    /* SINCRONIZACIÓN */
	    lecturaA = recv(nodoA->conecta, A, BYTES, FLAG_RECV);
	    lecturaB = recv(nodoB->conecta, B, BYTES, FLAG_RECV);
	    codificar(B, A, lecturaA);
	    send (emisor->conexion_actual, B, lecturaB, FLAG_ROUTER);
	    acumulado += lecturaA + lecturaB;
	    memset(B, 0, BYTES);
	    printf("\r Codificando ...");
	    fflush(stdout);
      }while(lecturaB > 0);

      CRONOMETRO();
      close(emisor->conexion_actual);     
      emisor->tam_buffer = acumulado;
      dpoint();
}

void
multicast_receptor_emisor (SOCKET *receptor, SOCKET *emisorA, SOCKET *emisorB) 
{
    assert(receptor && emisorA && emisorB);
    // Preparamos el modo Servidor
    preparar_servidor(emisorA);
    preparar_servidor(emisorB);
    // Aceptamos a los clientes
    aceptar(emisorA);
    aceptar(emisorB);
    // Conectamos con receptor
    conectar (receptor);
    // Inicializamos ...
    int lectura = 0; 
    size_t acumulado = 0;    
    receptor->buffer = malloc(BYTES);

    CRONOMETRO();
    do {
		lectura = recv(receptor->conecta, receptor->buffer, BYTES,  FLAG_RECV);
		//if (lectura > 0) { 
			send (emisorA->conexion_actual, receptor->buffer, lectura, FLAG_ROUTER);
			send (emisorB->conexion_actual, receptor->buffer, lectura, FLAG_ROUTER);
			//send (emisorA->conexion_actual, receptor->buffer, BYTES, 0); // OK!
			//send (emisorB->conexion_actual, receptor->buffer, BYTES, 0); // OK!
			memset(receptor->buffer, 0, BYTES);
			acumulado += lectura;
			printf(STR_MULTICAST_RECEPTOR_EMISOR_1, lectura, (int)acumulado);
		//}
			
	        fflush(stdout);
		
    }while(lectura > 0);
   
    CRONOMETRO();

    close(emisorA->conexion_actual);
    close(emisorB->conexion_actual);
 
    receptor->tam_buffer = acumulado;   

    if (lectura < 0) {
	destruir_socket(receptor);
	ERROR(STR_MULTICAST_RECEPTOR_EMISOR_2);
     }
    dpoint();
}


void
multicast (SOCKET *nodoA, SOCKET *nodoB)
{
    assert(nodoA && nodoB);
    crear_hilo((void *)&iniciar_servidor, (void *)nodoA);
    crear_hilo((void *)&iniciar_servidor, (void *)nodoB);
    lanzar_hilos();

    destruir_socket(nodoA);
    nodoA->buffer = NULL;
    nodoB->buffer = NULL;
    dpoint();
}

/**
 * Cea un SOCKET y transmite 5 vectores disepersales
 * apartir de la conf. en ../include/config.h
 * 
 * Cada vector es producto del algoritmo IDA
 */
void 
trasmitir5dispersos(char *ip_dispersal, 
                    int unsigned *puertos)
{
	size_t limit = 5;
	size_t tam = strlen(OUTPUT_PATH) + 3;
	int i = 0;
    	SOCKET disperso[limit];
	/*
	disperso[0] = nuevo_socket (IP_DISPERSAL, PUERTO_A);
	disperso[1] = nuevo_socket (IP_DISPERSAL, PUERTO_B);
	disperso[2] = nuevo_socket (IP_DISPERSAL, PUERTO_C);
	disperso[3] = nuevo_socket (IP_DISPERSAL, PUERTO_D);
	disperso[4] = nuevo_socket (IP_DISPERSAL, PUERTO_E);
	*/
	for (i = 0; i < limit ; i++) {
		/* Experimental */
		disperso[i] = nuevo_socket(ip_dispersal, puertos[i]);
		disperso[i].buffer = malloc(tam);
		assert(disperso[i].buffer);
		snprintf (disperso[i].buffer, tam, "%s/%d", OUTPUT_PATH, i);
    		crear_hilo((void *)&trasmitir_archivo, (void *)&disperso[i]);
	}

	lanzar_hilos();
	for (i = 0;  i < limit;  i++)
		destruir_socket(&disperso[i]);
    dpoint();
}


void
trasmitir_archivo (SOCKET *nodo) {
	assert(nodo && *(nodo->buffer));
	preparar_servidor(nodo);
	CRONOMETRO();
	aceptar(nodo);
        int file = open(nodo->buffer, O_RDONLY);
        char b[BYTES];
	memset(b, 0, BYTES);
	size_t acumulado = 0;
        int flag = 0;
        do {
    	    flag = read(file, b, BYTES);
	    send (nodo->conexion_actual, b, flag, FLAG_SEND);
	    acumulado += flag;
	    memset(b, 0, BYTES);
	    #ifdef LIMIT_SIZE 
	    	if (acumulado >= LIMIT_SIZE) 
			ERROR ("LIMITE EXCEDIDO");
	    #endif
        }while(flag > 0);
	
	CRONOMETRO();
        close(nodo->conexion_actual);
        close(file);
	nodo->tam_buffer = acumulado;
        dpoint();
 }

void
recibir_archivo (SOCKET *receptor) {
	puts("recibiendo archivo");
	assert(receptor && receptor->buffer);
	FILE *out = fopen(receptor->buffer, "wb");
	assert(out);
	CRONOMETRO();
	conectar(receptor);
	puts("conectado");
        int lectura = 0;
        int acumulado = 0; // Acumulado de Bytes recibidos
	char b[BYTES] = "\0";
	memset(b, 0, BYTES);
        do {
	      lectura = recv(receptor->conecta, b, BYTES, FLAG_RECV);
	      fwrite(b, lectura, 1, out);
	      memset(b, 0, BYTES);
	      acumulado += lectura;
	      #ifdef LIMIT_SIZE 
	    	  if (acumulado >= LIMIT_SIZE) 
		      ERROR ("LIMITE EXCEDIDO");
	      #endif
         } while(lectura > 0);
    
    	if (lectura < 0)  {
	    ERROR (STR_LEER_2);
	    destruir_socket(receptor);
	    }
       CRONOMETRO();
       receptor->tam_buffer = acumulado;
       fclose(out);
       out = NULL;
       dpoint();
 }


void
recibir2archivos (SOCKET *receptor1, SOCKET *receptor2) {
	FILE *out1 = fopen(receptor1->buffer, "wb");
	FILE *out2 = fopen(receptor2->buffer, "wb");
	assert(out1 && out2);
	CRONOMETRO();
	conectar(receptor1);
	conectar(receptor2);
        int lectura1 = 0;
        int lectura2 = 0;
        int acumulado = 0; // Acumulado de Bytes recibidos
	char b1[BYTES] = "\0";
	char b2[BYTES] = "\0";
	memset(b1, 0, BYTES);
	memset(b2, 0, BYTES);
        do {
	      lectura1 = recv(receptor1->conecta, b1, BYTES, FLAG_RECV);
	      lectura2 = recv(receptor2->conecta, b2, BYTES, FLAG_RECV );
	      fwrite(b1, lectura1, 1, out1);
	      fwrite(b2, lectura2, 1, out2);
	      memset(b1, 0, BYTES);
	      memset(b2, 0, BYTES);
	      acumulado += lectura1 + lectura2;
         } while(lectura2 > 0);
    
    	if (lectura1 < 0 || lectura2 < 0)  
	    ERROR (STR_LEER_2);
	    
       CRONOMETRO();
       receptor1->tam_buffer = acumulado;
       receptor2->tam_buffer = acumulado;
       fclose(out1);
       fclose(out2);

       out1 = out2 = NULL;
       dpoint();
 }


void
limpiar_dispersales(SOCKET *nodos) {
	SOCKET *nodo = nodos;
	for (; nodo->conexion_actual; nodo++) {
	      close(nodo->conexion_actual);
	      destruir_socket(nodo);
	}
	free(nodos);
	nodos = NULL;
	puts("Limpiando dispersales");
       dpoint();
}


void
router(SOCKET *receptor)
{
	assert(receptor);
	unsigned int port = PUERTO_PREDETERMINADO;
	unsigned int *puertos = (receptor->router ? receptor->router : &port);
	size_t clientes = receptor->clientes;
	assert(clientes);

	SOCKET nodo[clientes];
	int i = 0;
	for (; i < clientes && puertos[i]; i++) {
	    nodo[i] = nuevo_socket(LOCAL_HOST, puertos[i]);
	    printf("Nodo %d, puerto %u\n", i, puertos[i]);
	    preparar_servidor(&nodo[i]);
	    aceptar(&nodo[i]);
	}

	clientes = i;

	conectar(receptor);
	char buffer[BYTES] = "\0";
        memset(buffer,0, BYTES);
	size_t lectura = 0;
	size_t acumulado = 0; 
	char *grabar = receptor->buffer;
	if (!grabar)  {
		do {
		      lectura = recv(receptor->conecta, buffer, BYTES, FLAG_RECV);
		      for (i = 0; i < clientes; i++)
			      send(nodo[i].conexion_actual, buffer, lectura, FLAG_SEND); 
		      memset(buffer,0, BYTES);
		      acumulado += lectura;
		 } while(lectura > 0);
	}
	else {
		FILE *f = fopen(grabar,"wb");
		assert(f);
		do {
		      lectura = recv(receptor->conecta, buffer, BYTES, FLAG_RECV);
		      fwrite(buffer, lectura, 1, f);
		      for (i = 0; i < clientes; i++)
			      send(nodo[i].conexion_actual, buffer, lectura, FLAG_SEND); 
		      memset(buffer,0, BYTES);
		      acumulado += lectura;
		 } while(lectura > 0);

		 fclose(f);
		 f = NULL;
	}

	for (i = 0; i < clientes; i++) 
		close(nodo[i].conexion_actual);

  	receptor->tam_buffer = acumulado; 
        dpoint();

}

static char *
textUsuario (void) {
	  int i = 0;
	  static char c[BYTES] = "\0";
	  memset(c, 0,100);
	  char p = '\0';
	  printf("\n_");
	  while(i < BYTES &&  (p = getchar()) != '\n')
		   c[i++] = p;
	  return c;
}


void 
chat (SOCKET *socket, int mod) {
	assert(socket);
	char buffer[BYTES] = "\0";
	int flag = 0;
	switch (mod) {
		case ENVIAR:  
			preparar_servidor(socket);
			aceptar(socket);
			while(1) { 
			   puts("Enviar mensaje");
			   if ((send(socket->conexion_actual, textUsuario(), BYTES, 0)) <= 0)
				  ERROR("Cerro la conexion");
			   flag = recv(socket->conexion_actual, buffer, BYTES, 0);
			   if (flag) {
				printf("Cliente >> %s",buffer);
				memset (buffer, 0, flag);
			   }
			  fflush(stdin);

			  
			}
			break;

		case RECIBIR:  
			conectar(socket);
			while (1) {
				flag = recv(socket->conecta, buffer, BYTES, 0);
				printf("Server >> %s",buffer);
				memset (buffer, 0, flag);
			   	puts("Enviar mensaje");
			   	if ((send(socket->conecta, textUsuario(), BYTES, 0)) <= 0)
				    ERROR("Cerro la conexion");
			  fflush(stdin);
			}
			break;
		default: puts("Especifque el modo: ENVIAR | RECIBIR"); break;
				
	}
       
       dpoint();

}

/* define_receptor(int)
 * Crea un nodo T, acorde a la arquitectura de la red
 * devuelve un SOCKET 
 */

SOCKET 
define_receptorT(int mod) 
{
    SOCKET receptor;
    switch(mod) {
	 case '1':
            receptor = nuevo_socket (IP_C1, PUERTO_PREDETERMINADO);
           break;
	 case '2':
            receptor = nuevo_socket (IP_C2, PUERTO_PREDETERMINADO);
	    break;
	 default: ERROR("uso: ./intermedio n");
	    break;
	}

    dpoint();
    return receptor;
}


SOCKET
receptor_disperso(int mod) 
{	
    SOCKET receptor;
    switch(mod) {
	 case '1':receptor = nuevo_socket (IP_DISPERSAL, PUERTO_A);
                  break;
	 case '2':receptor = nuevo_socket (IP_DISPERSAL, PUERTO_B);
	          break;
	 case '4':receptor = nuevo_socket (IP_DISPERSAL, PUERTO_D);
	          break;
	 case '5':receptor = nuevo_socket (IP_DISPERSAL, PUERTO_E);
	          break;
	 default:ERROR("Argumentos admitidos:1-5");
	         break;
	   }

    dpoint();
    return receptor;
}

void 
p2p_codificador (SOCKET *a, SOCKET *b, SOCKET *c) 
{

   /// Nombre de los vectores deseados 
   a->buffer = "0";
   b->buffer = "tmp_xor"; 
   c->buffer = "2"; 
   
   
   /// Router: Recibir & Compartir 
   crear_hilo((void*)&router, (void*)a);
   crear_hilo((void*)&recibir_archivo, (void*)b);
   crear_hilo((void*)&recibir_archivo, (void*)c);
   lanzar_hilos();
   /// Decodificamos para obtener un vector disperso ("1")
   codificar2archivos("0", "tmp_xor", "1");

   dpoint();
}

void
p2p (SOCKET *a, SOCKET *b, SOCKET *c) 
{
   /// Nombre de los vectores deseados 
   a->buffer = "0";
   b->buffer = "1"; 
   c->buffer = "2"; 
   
   /// Asignación de los puertos para retrasmisión 
   a->clientes = 2;
   int unsigned puertos[2] = {PUERTO_D, PUERTO_E};
   a->router = puertos;

   /// Router: Recibir & Compartir 
   crear_hilo((void*)&router, (void*)a);
   crear_hilo((void*)&recibir_archivo, (void*)b);
   crear_hilo((void*)&recibir_archivo, (void*)c);
   lanzar_hilos();

   dpoint();
}

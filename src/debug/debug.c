#include "debug.h"

static FILE *f_log = NULL;

FILE *
__debug_file__(void) 
{	
	/* If you want to optimize this, return f_log only.
	 *     But don't forget set f_log */
	#ifdef __DEBUG_STREAM__
	       return (f_log ? f_log : __DEBUG_STREAM__);
	#else
	       return (f_log ? f_log : stderr);
	#endif
}

int
__debug_point_line__(int macro)
{
	return macro;
}

int
__debug_point__(char *process,int line,char *status,FILE *out) 
{	
	fprintf(out,"\n%s\tprocces_name=%s,code_line=%d,current_file=%s,status=%s",
	                          __DEBUG_MSG__,
	                                process,
			                   line,
		                         status,
	                  __DEBUG_ERROR_INFO__);
	return __debug_point_line__(line);
}

void
__debug_close_log__(void)
{	
	if(f_log && fclose(f_log) == EOF)
           perror("Error");
	f_log = NULL;
}

void
__debug_open_log__(char *path, char *mode) 
{	
	if(f_log) 
		return;
        FILE *new = fopen(path,mode);
	if(!new) 
	   return perror(path);
	f_log = new;
	fputs("\n---\n",f_log);
	fprintf(f_log,"%s\n\n",path);
	fflush(f_log);
}

void 
__debug_write_error__(char *type,char *expr,
                      char *process,char *in,int line, 
		      FILE *out)
{
	fputs("\b\n",out);
	fprintf(out,"%sevaluate_if(%s),process_name=",type,expr);
	fprintf(out, "%s,code_line=%d,current_file=%s,status=%s:\t%s",
	                              process,
				         line,
				           in,
		         __DEBUG_ERROR_INFO__,
		         __DEBUG_WARNG_MSG__);
	if(out == f_log)
	   __debug_write_error__(type,expr,process,in,line,stderr);
  
	errno = 0;
}


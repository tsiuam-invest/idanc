#include "util.h"

static CONTROL_HILOS C_I = NULL; ///< Var. lista tipo CONTROL_HILOS (inicio)
static CONTROL_HILOS C_F = NULL; ///< Var. lista tipo CONTROL_HILOS (fin)

void
crear_directorio (char *directorio) 
{
	 assert(directorio);
	 char path[500] = "\0";
	 memcpy(path, directorio, strlen(directorio));
	 char *resto = path;
         char *token= strtok_r (path, "/", &resto);
	 // Creando directorio 2 Niveles, permisos 755
	 mkdir(token, 0755);
	 mkdir(directorio, 0755);
         printf(STR_CREAR_DIRECTORIO_1,directorio);
}

char *
generar_nombre(char *nombre_archivo, char *dir) 
{
	assert (nombre_archivo);
        char *carpeta = (dir ? dir : "output"); // Por default "output"
        static char ruta_final[500] = "\0"; 
    	// Generamos el Timestamp del Archivo
    	time_t hora; 
    	time (&hora); // Inicializamos
    	struct tm *tiempo = localtime(&hora); // tiempo local CPU
        sprintf(ruta_final, 
	    STR_GRABAR_BUFFER_ARCHIVO_1,
            carpeta, tiempo->tm_mday, 
            tiempo->tm_mon, (int unsigned) tiempo->tm_year - 100, // Conversión del año
	    tiempo->tm_hour, tiempo->tm_min, tiempo->tm_sec,
            nombre_archivo);
	return ruta_final;
}

char *
concat(char *a, char *b) 
{	
	assert(a && b);
	static char c[1000] = "\0";
	snprintf(c, 1000, "%s%s", a, b);
	return c;
}

CONTROL_HILOS
nuevo (void)  
{
	CONTROL_HILOS n = malloc(sizeof(struct CONTROL_HILOS));
	assert(n);
	n->sig = NULL;
	printf(STR_CONTROL_HILOS_1);
	fflush(stdout);
	return n;
}

void 
crear_hilo (void *ptr_func(), void *ptr_para)
{	
	assert(ptr_func && ptr_para);
	CONTROL_HILOS n = nuevo();
	pthread_create(&n->hilo, NULL, ptr_func, ptr_para); 
	if (!C_I) 
		C_F = C_I = n;
	else 
		C_F = C_F->sig = n;
	
}

void 
lanzar_hilos(void) 
{
	printf(STR_LANZAR_HILOS_1);
	fflush(stdout);
	CONTROL_HILOS ptr = C_I;
	CONTROL_HILOS aux = NULL;
	while (ptr) {
		pthread_join(ptr->hilo, NULL);
		//printf("\rSiguiendo al hilo PID=%lo             ", ptr->hilo);
		//fflush(stdout);
		aux = ptr->sig;
		free(ptr);
		ptr = aux;
	}
	C_F = C_I = NULL;
	ptr = aux = NULL;
}


char *
xtoken (char *v, char *token, size_t limit) {
	assert(v);
	static char cadena[1000];
	memccpy(cadena, v, '\0', 1000);
	char *ptr = cadena;
	char *salida;
	int i;
	for (i = 0; i < limit + 1; i++ )  {
		salida = strtok_r(ptr, token,&ptr);
		assert(salida);
		}
        return salida;
}

size_t 
obtener_tam_archivo(char *ruta) 
{
	FILE *f = fopen(ruta,"rw");
	EXIT_IF(!f);
	size_t tam = 0;
	fseek(f,0,SEEK_END);
	tam = ftell(f);
	fclose(f);
	f = NULL;
	return tam;
}


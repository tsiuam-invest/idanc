#include "tiempo.h"

static CLOCK REALTIME = {0};
BITACORA cronometro;

/* *
 * Inicializa los tributos de un struct BITACORA
 *
 * BITACORA.nombre_nodo
 * BITACORA.tam 
 */
void
iniciar_bitacora (char *nombre) 
{
     cronometro.nombre_nodo  =  (nombre) ? nombre:NULL;
     cronometro.tam = 0;
}

/**
 *  Imprime el tiempo transcurrido 
 *  para seguimiento en un proceso del sistem
 */
void
imprime_tiempo_trans (int linea, char *fichero) 
{	
      printf(STR_IMPRIME_TIEMPO_TRANS_1,cronometro.crono, linea , fichero);
}

/**
 * Calcula el tiempo transcurrido
 * desde el primer conteo 
 */
void
calc_tiempo_trans (void) 
{	
     if (cronometro.crono > 0)
	 cronometro.crono = ( ((double) clock() - cronometro.crono) / CLOCKS_PER_SEC) * .600;
     
     else  {
	 cronometro.crono = (double) clock();
     }
     

     //MUESTRA_CRONOMETRO();
}

void 
cronometro_relog() 
{	
	struct timespec actual;
	clock_gettime(CLOCK_REALTIME, &actual);
	if(!REALTIME.cuenta) {
		REALTIME.cuenta = 1;
		REALTIME.inicio = actual;
	}
	else { 
		REALTIME.fin.tv_nsec = actual.tv_nsec - REALTIME.inicio.tv_nsec;
		REALTIME.fin.tv_sec = actual.tv_sec  - REALTIME.inicio.tv_sec;
		 if (REALTIME.fin.tv_sec > 0 && REALTIME.fin.tv_nsec < 0){
			REALTIME.fin.tv_nsec += NANO_SEGUNDOS;
			REALTIME.fin.tv_sec--;
		    }
		 else if (REALTIME.fin.tv_sec < 0 && REALTIME.fin.tv_nsec > 0){
			REALTIME.fin.tv_nsec -= NANO_SEGUNDOS;
			REALTIME.fin.tv_sec++;
		    }

	}
	printf("\b\fTiempo trascurrido:");
	printf("\t%d.%.9ld segundos\n", (int)REALTIME.fin.tv_sec,REALTIME.fin.tv_nsec);

	calc_tiempo_trans(); 	
}

/**
 *  Toma el tiempo calculado por CRONOMETRO
 *
 *  Dentro del dir CREAR_LOG 
 *
 *  Graba un log con formato CSV
 */
void
guardar_bitacora (char *nombre_nodo) 
{	
     #ifdef CREAR_LOG
       MUESTRA_CRONOMETRO();	
       crear_directorio(concat("../",CREAR_LOG));
       char *nombre_archivo = FILE_NAME;
       size_t tam = cronometro.tam;
       char file[200] = "\0";
       snprintf(file, 200, "../%s/%s.csv",CREAR_LOG, nombre_nodo); 
       FILE *bitacora = fopen(file, "a+");
       assert (bitacora);
       time_t hora;
       time (&hora);
       struct tm *tiempo = localtime(&hora); // tiempo local CPU
       //fprintf(bitacora, "%f segs|%s|%lu bytes|PROCESO_%s|%d/%d/%d|%d:%d:%d|%s\n", 
       fprintf(bitacora, "%d.%9ld segs Reales|%f segs CPU|%s|%lu bytes|PROCESO_%s|%d/%d/%d|%d:%d:%d|%s\n", 
		    //cronometro.crono,
		   (int)REALTIME.fin.tv_sec, REALTIME.fin.tv_nsec,
		   cronometro.crono,
		    //
		    nombre_archivo,
		    tam,
		    #ifdef PAQUETE_OFUSCADO
		        LAMBDA(PAQUETE_OFUSCADO),
		    #elif defined(PAQUETE_CIFRADO)
			LAMBDA(PAQUETE_CIFRADO),
		    #elif defined(PAQUETE_CIFRADO_INTERMEDIO)
			LAMBDA(PAQUETE_CIFRADO_INTERMEDIO),
		    #else
			"SIMPLE_CODIFICACION",
		    #endif
		    tiempo->tm_mday, 
		    tiempo->tm_mon, 
		    (int unsigned) tiempo->tm_year - 100, // Conversión del año
		    tiempo->tm_hour, 
		    tiempo->tm_min, 
		    tiempo->tm_sec,
		    nombre_nodo
		    );

	puts(STR_GUARDAR_BITACORA_2);
	fclose(bitacora);
	bitacora = NULL;
      #endif

}







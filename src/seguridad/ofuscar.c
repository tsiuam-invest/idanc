#include "ofuscar.h"

/*
 * Invierte una cadena
*/
char *
invierte(char *cadena,size_t longitud)
{	
	EXIT_IF(!cadena);
	int i, j;
	char aux = '\0';
	j = longitud - 1;
	for (i = 0; i < j; i++, j--) { 
		aux = cadena[i];
		cadena[i] = cadena[j];
		cadena[j] = aux;
	}
	return cadena;
        /*********************************************
	// Preservando mem. alojada en cadena

	char *invertida = malloc (longitud);
	// Debemaos validar el buffer
	assert (invertida);
	for (i = longitud-1, j = 0; i >= 0; i--, j++) {
		//if (cadena[i] != '\0')
		   // Invertimos la cadena
		   invertida [j] = cadena [i];
	}

	//invertida[j] = '\0';
	return invertida;
 	**********************************************/
}

static char *
invierte_ascii(char *x, char *y,size_t longitud,int polo)
{	
	EXIT_IF(!x || !y);
	int i, j, k;
	char aux = '\0';
	j = longitud - 1;
	if (!j) 
		*y = *x + (polo ^ 256);
	k = longitud;
	for (i = 0; i < j; i++, j--, k--) { 
		aux = x[i]  +  polo * k;
		y[i] = x[j] + polo * k;
		y[j] = aux;
	}
	return y;
}

char * 
ofuscar_ascii(char *x, char *y, size_t t) 
{
	return invierte_ascii(x,y,t,1);
}

char *
des_ofuscar_ascii(char *x, char *y, size_t t) 
{
	return invierte_ascii(x,y,t,-1);
}

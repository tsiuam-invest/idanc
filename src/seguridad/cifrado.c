#include "cifrado.h"

/* *
 * liberar_manejador_seguro (gcry_cipher_hd):
 * Limpia memoria para el objeto de Gcry
 *
*/
static void 
liberar_manejador_seguro(gcry_cipher_hd_t *hd) 
{
       EXIT_IF(!hd);
       // Liberamos el handle-manejador
       gcry_cipher_close(*hd);
       hd = NULL;
       STR_LIBERAR_MANEJADOR_SEGURO_1;
}

/**
 * Evalua el error mediante el manejador libgpg-error.
 * Imprimiendo en el flujo stderr del sistema
 *
 */
static void 
evaluar_error(gcry_error_t error) 
{
       if(error){
	       fprintf(stderr,"\b\f");
               fprintf(stderr, RED("Lybgcrypt error:\t%s:%s"),
	               gcry_strsource(error),
                       gcry_strerror(error)
		      );
	       fprintf(stderr,"\n");
	       exit(EXIT_FAILURE);
         }
}

/**
 * Recibe un apuntador del manejador de cifrado
 * para configurar el vector usado para el cifrado
 *
 **/
static void
configurar_llave_cifrado(gcry_cipher_hd_t *hd)
{	
	EXIT_IF(!hd);
	/**
	 * Dependiendo el modo de cifrado, se podría llegar a ocupar
	 * un vector de longitud K. 
	 */

	//char vector_inicial[LONGITUD_K] = {0};
	//memset(vector_inicial,0,LONGITUD_K);
        size_t tam_key  = gcry_cipher_get_algo_keylen(AES);
	// printf("\nTam_key = %d\n", (int) tam_key); // Tam del vector de la llave
	gcry_error_t error =  gcry_cipher_setkey(*hd, PASSWORD, tam_key);
	evaluar_error (error);
	//error = gcry_cipher_setiv(*hd, vector_inicial, LONGITUD_K);
	//evaluar_error(error);
	STR_CONFIGURAR_LLAVE_CIFRADO_1;
}

/**
 * Crea un objeto para el manejador seguro de cifrado
 * de biblioteca Libgcryp
 * 
 * Prapara la inicialización de memoria segura
 */

static gcry_cipher_hd_t 
crear_manejador_seguro (void)
{

        //gcry_control (GCRYCTL_INIT_SECMEM, 16384, 0); 
	gcry_control (GCRYCTL_RESUME_SECMEM_WARN);
        gcry_control (GCRYCTL_INITIALIZATION_FINISHED, 0);
	EXIT_IF(!gcry_control(GCRYCTL_INITIALIZATION_FINISHED_P));
  	gcry_cipher_hd_t hd;
	gcry_error_t error = gcry_cipher_open(&hd, AES, MODE, FLAG);
	evaluar_error (error);
	configurar_llave_cifrado(&hd);
	STR_CREAR_MANEJARO_SEGURO_2;
	return hd;
}


/**
 *  Prepara los métodos necesarios para cifrar un vector
 *  con longitud especificada.
 *  El sistema solicita memoria para proteger el vector original
 *  Regresa un vector cifrado validado
 */
void
cifrar_buffer (char *entrada,char *salida,size_t tam) 
{	
        gcry_cipher_hd_t hd = crear_manejador_seguro ();
	gcry_error_t error = gcry_cipher_encrypt(hd,       // Manejador
			                          salida,  // Buffer de Salida
					          tam ,    // Tam de la Salida
				                  entrada, // Buffer de Entrada
						  tam      // Tam de Entrada
						  );
	evaluar_error (error);
	STR_CIFRAR_BUFFER_1;
	liberar_manejador_seguro (&hd);
}

/** 
 *  Prepara los métodos necesarios para descifrar un vector 
 *  con longitud especificada.
 *  El sistema solicita memoria para proteger el vector original
 *  Regresa un vector descifrado validado
 */
void 
des_cifrar_buffer (char *entrada,char *salida,size_t tam) 
{	
        gcry_cipher_hd_t hd = crear_manejador_seguro ();
	gcry_error_t error = gcry_cipher_decrypt(hd,     // Manejador
			                         salida, // Buffer de Salida
						 tam,    // Longitud del vector de Salida
						 entrada,// Buffer de Entrada
						 tam     // Longitud del buffer de Entrada
						 );
	evaluar_error (error);
	STR_DES_CIFRAR_BUFFER_1;
        liberar_manejador_seguro(&hd);
}


static void
seguridad_archivo(char *nombre_archivo,char *salida,
                  gcry_error_t(*cripto)()) 
{	
	EXIT_IF(!nombre_archivo || !salida);
	EXIT_IF(!strcmp(nombre_archivo,salida));
	FILE *archivo = fopen(nombre_archivo,"rb");
	FILE *nuevo_archivo = fopen(salida,"wb");
	EXIT_IF(!archivo || !nuevo_archivo);
	int lectura = 0;
	char x[BYTES] = "\0";
	char y[BYTES] = "\0";
        gcry_cipher_hd_t hd = crear_manejador_seguro();
	do{
	   lectura = fread(x,1,BYTES,archivo);
	   EXIT_IF(lectura < 0);
	   evaluar_error(cripto(hd,y,lectura,x,lectura));
	   fwrite(y,lectura,1,nuevo_archivo);
	   memset(x,0,BYTES);
	   memset(y,0,BYTES);
	}while(lectura > 0);
	fclose(archivo);
	fclose(nuevo_archivo);
	archivo = NULL;
	nuevo_archivo = NULL;
        liberar_manejador_seguro(&hd);
}

static void
proto_ofusca(char *nombre_archivo, char *salida,
		  gcry_error_t(*cripto)(),
		  char *(*protocolo)()) 
{	
	EXIT_IF(!nombre_archivo || !salida);
	EXIT_IF(!strcmp(nombre_archivo,salida));
	FILE *archivo = fopen(nombre_archivo,"rb");
	FILE *nuevo_archivo = fopen(salida,"wb");
	EXIT_IF(!archivo || !nuevo_archivo);
	int lectura = 0;
	char x[BYTES]  = "\0";
	char y[BYTES]  = "\0";
        gcry_cipher_hd_t hd = crear_manejador_seguro ();
	size_t mitad = obtener_tam_archivo(nombre_archivo);
	EXIT_IF(!mitad);
	do{
	   lectura = fread(x,1,BYTES,archivo);
	   EXIT_IF(lectura < 0);
	   if(lectura < mitad) 
	   	evaluar_error(cripto(hd,y,lectura,x,lectura));
	   else
		protocolo(x,y,lectura);
	   fwrite(y,lectura,1,nuevo_archivo);
	   memset(x,0,BYTES);
	   memset(y,0,BYTES);
	}while(lectura > 0);
	fclose(archivo);
	fclose(nuevo_archivo);
	archivo = NULL;
	nuevo_archivo = NULL;
        liberar_manejador_seguro(&hd);
}

void
cifrar_archivo(char *x, char *y)
{
	seguridad_archivo(x,y,gcry_cipher_encrypt);
}

void
des_cifrar_archivo(char *x, char *y)
{
	seguridad_archivo(x,y,gcry_cipher_decrypt);
}


void 
ofuscar_archivo(char *x, char *y)
{
	proto_ofusca(x,y,gcry_cipher_encrypt,
		     ofuscar_ascii);
}
void 
des_ofuscar_archivo(char *x, char *y)
{
	proto_ofusca(x,y,gcry_cipher_decrypt,
		     des_ofuscar_ascii);
}


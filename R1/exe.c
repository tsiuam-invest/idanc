/*!\file 
 * Nodo receptor etiqueta R1.
 * uso: ./exe 
 */

#include "../includes/config.h"

int main(void)
{   
   
   dauto();  
   SOCKET a = nuevo_socket(IP_N1, PUERTO_A);
   SOCKET b = nuevo_socket(IP_T1, PUERTO_A);
   SOCKET c = nuevo_socket(IP_R5, PUERTO_PREDETERMINADO);  
   p2p_codificador(&a,&b,&c);

   /// Recupera el vector fuente
   recupera("recuperado_"FILE_NAME);
   
   /// Limpiar
   remove("tmp_xor");
   guardar_bitacora("R1");

   return 0;
}

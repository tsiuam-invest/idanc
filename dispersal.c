/*!\file
 *  Módulo: dispersal.c 
 *  Crea 5 vectores dispersos.
 *  Vector fuente especificado en includes/config.h.
 *  Trasmisión acorde en includes/config.h.
 *  uso: ./dispersal
 */
#include "config.h"

int main(int argc, char *argv[])
{
   
   if(argc < 2) {
	   printf("Nodo dispersal\n");
	   printf("%s RutaArchivo RutaDispersados 5PUERTOS\n", argv[0]);
	   printf("\tEjemplo:");
	   printf("%s dir_archivo dir_disper 1500:2200:3010:4000:1600\n", argv[0]);
	   return -1;
	   }
    
    char *archivo = argv[1];
    char *salida  = argv[2];
    char *puertos = argv[3];
     

    int  i = ida_dispersa(archivo,salida,".data");
    printf("%d bytes dispersados en %s\n",i,salida);
    SOCKET disperso[5];
    
    for (i= 0; i < 5; i++)  {
	 disperso[i] = nuevo_socket(LOCAL_HOST,TO_UINT(xtoken(puertos,":",i))); 
         disperso[i].buffer = (char *)malloc(sizeof(char) * 256);
	 snprintf(disperso[i].buffer,255,"%s/%d.%s",salida,i,".data");
         crear_hilo((void *)&trasmitir_archivo, (void *)&disperso[i]);
	 }

    
    lanzar_hilos();

    return 0;
}

/*!\file
 * Binario codificador.
 * Comprueba el xoring de dos vectores
 * producido por el sistema: a + b = c.
 *
 * uso: 
 * 
 * ./codificador InfileA InfileB OutFileC
 */

#include "config.h"            
 
int main(int argc, char *argv[])  {

    codificar2archivos(argv[1],argv[2],argv[3]);
   
    return 0;
 
}

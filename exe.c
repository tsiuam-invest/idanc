/*!\file 
 * Nodo receptor etiqueta R1.
 * uso: ./exe 
 */

#include "../includes/config.h"

int main(void)
{   
   
   SOCKET A = nuevo_socket(IP_N1, PUERTO_A);
   SOCKET B = nuevo_socket(IP_N2, PUERTO_A);
   SOCKET C = nuevo_socket(IP_N3, PUERTO_A);
   SOCKET D = nuevo_socket(IP_N4, PUERTO_A);
   SOCKET E = nuevo_socket(IP_N5, PUERTO_A);

   A.buffer = "0";
   B.buffer = "1";
   C.buffer = "2";
   D.buffer = "3";
   E.buffer = "4";

   crear_hilo((void*)&recibir_archivo,(void*)&A);
   crear_hilo((void*)&recibir_archivo,(void*)&B);
   crear_hilo((void*)&recibir_archivo,(void*)&C);
   crear_hilo((void*)&recibir_archivo,(void*)&D);
   crear_hilo((void*)&recibir_archivo,(void*)&E);
   lanzar_hilos();
   sleep(5);

   recupera();

   guardar_bitacora("R1");

   return 0;
}
